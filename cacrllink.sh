#!/bin/sh
#
# script for creating links to certificates and CRLs in the current directory
#
# Author: Jan Dittberner <jan@dittberner.info>
# Date:   2011-05-04

for cert in *.crt.pem
do
    ln -s "${cert}" `openssl x509 -in "${cert}" -noout -hash`.0
done

for crl in *.crl.pem
do
    ln -s "${crl}" `openssl crl -in "${crl}" -noout -hash`.r0
done
