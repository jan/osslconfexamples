#!/bin/bash

set -e

usage() {
  echo "Usage: $1 <cabasedir> <ca.key.pem> <ca.crt.pem>"
  exit 1
}

if [ $# -lt 3 ]; then
  usage $0
fi

if [ ! -f "$2" ]; then
  echo "$2 is no file."
  usage $0
fi
REQCONF="$2"

if [ ! -f "$3" ]; then
  echo "$2 is no file."
  usage $0
fi
CACONF="$3"

if [ -d "$1" ]; then
  echo "$1 does already exist. Please specify a new directory."
  usage $0
fi
CADIR="$1"

mkdir -p "${CADIR}"/{certs,crl,newcerts,private}

cp "$2" "${CADIR}/private/ca.key.pem"
cp "$3" "${CADIR}/ca.crt.pem"

echo "01" > "${CADIR}/serial"
touch "${CADIR}/index.txt"
