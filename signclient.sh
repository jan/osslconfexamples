#!/bin/bash

if [ $# -lt 1 ]; then
    echo "Usage: $0 <client-csr.csr.pem>"
    exit 1
fi

openssl ca -config subca.conf -in "$1" -out "${1%%.csr.pem}.crt.pem" -extensions client_cert
