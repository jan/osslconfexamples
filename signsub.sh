#!/bin/bash

if [ $# -lt 1 ]; then
    echo "Usage: $0 <subca-request.csr.pem>"
    exit 1
fi

openssl ca -config rootca.conf -in "$1" -out "${1%%.csr.pem}.crt.pem"
